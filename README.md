# repl calc

Very basic calculator feature-wise but implemented by actually parsing an expression.

There are minimal memory allocations in the lexing stage, every token and error data derived from the input buffer is (nightmarishily) borrowed from it.

The parsing stage uses box to construct the AST (using the heap is unavoidable) and clones the tokens for the errors because I didn't want to make a mess of the code by adding an additional lifetime to `parser::Error` to avoid like 32 bytes of stack.

## Usage (why would you tho)

- `-repl` - evaluate line-by-line expressions in a read-eval-print-loop from stdin.
- `-stdin` - evaluate a single expression from stdin.
- `-eval <expression>` - evaluate 'expression'.
- (nothing) - run the repl.
