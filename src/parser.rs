use std::ops::{Add, Div, Mul, Neg, Sub};

use crate::{
    cursor::{AcceptError, Cursor},
    Token, TokenKind, TokenStream,
};

use derive_more::Display;

/*
    Start -> Expression EoF ;

    Expression -> Term ;

    Term -> Mul ( ( '-' | '+' ) Mul )* ;

    Mul -> Div ( ( '*' ) Div )* ;

    force higher precedence for Div to correctly parse x/y*z
    Div -> Unary ( '/' Unary )* ;

    Unary -> '-' Unary | Primary;

    Primary -> '(' Expression ')' | float(f) | int(i) ;
*/

#[derive(Debug, Clone)]
pub enum ErrorKind<'a> {
    EndOfExpression,
    Unexpected(Token<'a>),
}

#[derive(Debug, Clone)]
pub struct Error<'a> {
    pub kind: ErrorKind<'a>,
    pub text: String,
}

impl<'a> Error<'a> {
    fn end_of_expression(expected: &str) -> Self {
        Self {
            kind: ErrorKind::EndOfExpression,
            text: format!("expected: {expected} [got: None]"),
        }
    }

    fn unexpected(expected: &str, got: &Token<'a>) -> Self {
        Self {
            kind: ErrorKind::Unexpected(got.clone()),
            text: format!("expected: {expected} [got '{}']", got.kind),
        }
    }

    fn pprint(&self, input: &str) {
        println!();

        match self.kind {
            ErrorKind::EndOfExpression => {
                let col = input.len() - 1;
                let space_pad = " ".repeat(col);

                eprintln!("error: unexpected end of expression");
                eprintln!("\t{}", input.trim_end());
                eprintln!("\t{space_pad}^ {}", &self.text);
            }

            ErrorKind::Unexpected(ref tok) => {
                let col = tok.position.start;
                let space_pad = " ".repeat(col);
                let arrow_pad = "^".repeat(tok.position.len());

                eprintln!("error: unexpected token '{}'", tok.kind);
                eprintln!("\t{}", input.trim_end());
                eprintln!("\t{space_pad}{arrow_pad} {}", &self.text);
            }
        }
    }
}

#[derive(Debug, Display, Clone)]
pub enum Literal {
    #[display(fmt = "{} :: Int", "_0")]
    Int(i64),

    #[display(fmt = "{} :: Float", "_0")]
    Float(f64),
}

impl Neg for Literal {
    type Output = Literal;

    fn neg(self) -> Self::Output {
        match self {
            Self::Int(i) => Self::Int(-i),
            Self::Float(f) => Self::Float(-f),
        }
    }
}

impl Add for Literal {
    type Output = Literal;

    fn add(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Self::Int(i), Self::Int(j)) => Self::Int(i + j),
            (Self::Float(f), Self::Float(g)) => Self::Float(f + g),
            (Self::Float(f), Self::Int(i)) => Self::Float(f + i as f64),
            (Self::Int(i), Self::Float(f)) => Self::Float(f + i as f64),
        }
    }
}

impl Sub for Literal {
    type Output = Literal;

    fn sub(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Self::Int(i), Self::Int(j)) => Self::Int(i - j),
            (Self::Float(f), Self::Float(g)) => Self::Float(f - g),
            (Self::Float(f), Self::Int(i)) => Self::Float(f - i as f64),
            (Self::Int(i), Self::Float(f)) => Self::Float(f - i as f64),
        }
    }
}

impl Mul for Literal {
    type Output = Literal;

    fn mul(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Self::Int(i), Self::Int(j)) => Self::Int(i * j),
            (Self::Float(f), Self::Float(g)) => Self::Float(f * g),
            (Self::Float(f), Self::Int(i)) => Self::Float(f * i as f64),
            (Self::Int(i), Self::Float(f)) => Self::Float(f * i as f64),
        }
    }
}

impl Div for Literal {
    type Output = Literal;

    fn div(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Self::Int(i), Self::Int(j)) => Self::Int(i / j),
            (Self::Float(f), Self::Float(g)) => Self::Float(f / g),
            (Self::Float(f), Self::Int(i)) => Self::Float(f / i as f64),
            (Self::Int(i), Self::Float(f)) => Self::Float(f / i as f64),
        }
    }
}

#[derive(Debug, Clone)]
pub enum Expression {
    Literal(Literal),
    Group(Box<Expression>),
    Negate(Box<Expression>),
    Add(Box<Expression>, Box<Expression>),
    Sub(Box<Expression>, Box<Expression>),
    Mul(Box<Expression>, Box<Expression>),
    Div(Box<Expression>, Box<Expression>),
}

impl<'a> Expression {
    pub fn interpret(self) -> Literal {
        match self {
            Self::Literal(lit) => lit,
            Self::Group(expr) => expr.interpret(),
            Self::Negate(expr) => -expr.interpret(),
            Self::Add(x, y) => x.interpret() + y.interpret(),
            Self::Sub(x, y) => x.interpret() - y.interpret(),
            Self::Mul(x, y) => x.interpret() * y.interpret(),
            Self::Div(x, y) => x.interpret() / y.interpret(),
        }
    }

    fn start(cursor: &mut Cursor<'a, Token<'a>>) -> Result<Box<Self>, Error<'a>> {
        let expression = Self::expression(cursor);

        cursor
            .accept(|tok| tok.is_of_kind(TokenKind::EoF))
            .map_err(|err| match err {
                AcceptError::DidNotMatch(got) => Error::unexpected("'+' '-' '*' '/'", got),
                AcceptError::StreamExhausted => unreachable!("wouldn't gliding be faster"),
            })?;

        expression
    }

    fn expression(cursor: &mut Cursor<'a, Token<'a>>) -> Result<Box<Self>, Error<'a>> {
        Self::term(cursor)
    }

    fn term(cursor: &mut Cursor<'a, Token<'a>>) -> Result<Box<Self>, Error<'a>> {
        let mut expr = Self::mul(cursor)?;

        while let Ok(op) =
            cursor.accept(|tok| tok.is_of_kind(TokenKind::Plus) || tok.is_of_kind(TokenKind::Minus))
        {
            let rhs = Self::mul(cursor)?;

            expr = match op.kind {
                TokenKind::Plus => Box::new(Expression::Add(expr, rhs)),
                TokenKind::Minus => Box::new(Expression::Sub(expr, rhs)),
                _ => unreachable!("cryo is always far more effective in the rain"),
            }
        }

        Ok(expr)
    }

    fn mul(cursor: &mut Cursor<'a, Token<'a>>) -> Result<Box<Self>, Error<'a>> {
        let mut expr = Self::div(cursor)?;

        while cursor
            .accept(|tok| tok.is_of_kind(TokenKind::Multiply))
            .is_ok()
        {
            let rhs = Self::div(cursor)?;
            expr = Box::new(Expression::Mul(expr, rhs));
        }

        Ok(expr)
    }

    fn div(cursor: &mut Cursor<'a, Token<'a>>) -> Result<Box<Self>, Error<'a>> {
        let mut expr = Self::unary(cursor)?;

        while cursor
            .accept(|tok| tok.is_of_kind(TokenKind::Divide))
            .is_ok()
        {
            let rhs = Self::unary(cursor)?;
            expr = Box::new(Expression::Div(expr, rhs));
        }

        Ok(expr)
    }

    fn unary(cursor: &mut Cursor<'a, Token<'a>>) -> Result<Box<Self>, Error<'a>> {
        if cursor.accept(|tok| tok.kind == TokenKind::Minus).is_ok() {
            Ok(Box::new(Self::Negate(Self::unary(cursor)?)))
        } else {
            Self::primary(cursor)
        }
    }

    fn primary(cursor: &mut Cursor<'a, Token<'a>>) -> Result<Box<Self>, Error<'a>> {
        if let Ok(tok) = cursor.accept(Token::is_numeric) {
            let lit = match tok.kind {
                TokenKind::Int(x) => Literal::Int(x.parse().unwrap()),
                TokenKind::Float(x) => Literal::Float(x.parse().unwrap()),
                _ => unreachable!("huh? must've been the wind"),
            };

            return Ok(Box::new(Self::Literal(lit)));
        }

        cursor
            .accept(|tok| tok.is_of_kind(TokenKind::LParen))
            .map_err(|err| match err {
                AcceptError::StreamExhausted => Error::end_of_expression("'('"),
                AcceptError::DidNotMatch(tok) => Error::unexpected("'('", tok),
            })?;

        let expression = Self::expression(cursor)?;

        cursor
            .accept(|tok| tok.is_of_kind(TokenKind::RParen))
            .map_err(|err| match err {
                AcceptError::StreamExhausted => Error::end_of_expression("')'"),
                AcceptError::DidNotMatch(tok) => Error::unexpected("')'", tok),
            })?;

        Ok(Box::new(Self::Group(expression)))
    }
}

pub type ParserResult<'a> = Result<Box<Expression>, Error<'a>>;

pub fn parse_to_ast<'a>(ts: &'a TokenStream) -> ParserResult<'a> {
    let mut cursor = Cursor::new(ts);

    Expression::start(&mut cursor)
}

pub fn error(input: &str, error: &Error) {
    error.pprint(input)
}
