use std::ops::Range;

use crate::cursor::Cursor;

use derive_more::Display;

#[derive(Debug, Display, Clone, PartialEq)]
pub enum TokenKind<'a> {
    #[display(fmt = "Int({})", _0)]
    Int(&'a str),

    #[display(fmt = "Float({})", _0)]
    Float(&'a str),

    #[display(fmt = "+")]
    Plus,

    #[display(fmt = "-")]
    Minus,

    #[display(fmt = "/")]
    Divide,

    #[display(fmt = "*")]
    Multiply,

    #[display(fmt = "(")]
    LParen,

    #[display(fmt = ")")]
    RParen,

    #[display(fmt = "EOF")]
    EoF,
}

impl TokenKind<'static> {
    pub const INT_KIND: Self = TokenKind::Int("0");
    pub const FLOAT_KIND: Self = TokenKind::Float("0.0");
}

#[derive(Debug, Clone)]
pub struct Token<'a> {
    pub kind: TokenKind<'a>,
    pub position: Range<usize>,
}

impl<'a> Token<'a> {
    pub fn is_of_kind(&self, kind: TokenKind) -> bool {
        use TokenKind::*;

        match (&self.kind, kind) {
            (Int(_), Int(_)) => true,
            (Float(_), Float(_)) => true,
            (p, ref q) if p == q => true,
            _ => false,
        }
    }

    pub fn is_numeric(&self) -> bool {
        self.is_of_kind(TokenKind::INT_KIND) || self.is_of_kind(TokenKind::FLOAT_KIND)
    }

    fn plus(cursor: &Cursor<'a, u8>) -> Self {
        let pos = cursor.pos() - 1;

        Self {
            kind: TokenKind::Plus,
            position: pos..pos + 1,
        }
    }

    fn minus(cursor: &Cursor<'a, u8>) -> Self {
        let pos = cursor.pos() - 1;

        Self {
            kind: TokenKind::Minus,
            position: pos..pos + 1,
        }
    }

    fn multiply(cursor: &Cursor<'a, u8>) -> Self {
        let pos = cursor.pos() - 1;

        Self {
            kind: TokenKind::Multiply,
            position: pos..pos + 1,
        }
    }

    fn divide(cursor: &Cursor<'a, u8>) -> Self {
        let pos = cursor.pos() - 1;

        Self {
            kind: TokenKind::Divide,
            position: pos..pos + 1,
        }
    }

    fn lparen(cursor: &Cursor<'a, u8>) -> Self {
        let pos = cursor.pos() - 1;

        Self {
            kind: TokenKind::LParen,
            position: pos..pos + 1,
        }
    }

    fn rparen(cursor: &Cursor<'a, u8>) -> Self {
        let pos = cursor.pos() - 1;

        Self {
            kind: TokenKind::RParen,
            position: pos..pos + 1,
        }
    }

    fn number(cursor: &mut Cursor<'a, u8>) -> Self {
        let start = cursor.pos() - 1;

        let mut is_float = false;

        while let Some(d) = cursor.peek().copied() {
            if d.is_ascii_digit() {
                cursor.advance();
            } else if d == b'.' {
                let Some(next) = cursor.peek_next().copied() else {
                    break;
                };

                if !next.is_ascii_digit() {
                    break;
                }

                is_float = true;
                cursor.advance();
            } else {
                break;
            }
        }

        let end = cursor.pos();

        // Safety: every symbol in range is ASCII, thus valid UTF-8
        let slice = unsafe { std::str::from_utf8_unchecked(&cursor[start..end]) };

        Self {
            position: start..end,
            kind: if is_float {
                TokenKind::Float(slice)
            } else {
                TokenKind::Int(slice)
            },
        }
    }

    fn eof(cursor: &Cursor<'a, u8>) -> Self {
        let pos = cursor.pos() - 1;

        Self {
            kind: TokenKind::EoF,
            position: pos..pos + 1,
        }
    }
}

pub type TokenStream<'a> = Vec<Token<'a>>;

#[derive(Debug, Clone, PartialEq)]
pub enum ErrorKind<'a> {
    InvalidSequence(&'a str),
}

#[derive(Debug, Clone)]
pub struct Error<'a> {
    pub kind: ErrorKind<'a>,
    pub position: Range<usize>,
}

fn is_valid_symbol(sym: u8) -> bool {
    b"()*+-/0123456789".binary_search(&sym).is_ok()
}

impl<'a> Error<'a> {
    fn invalid_sequence(cursor: &mut Cursor<'a, u8>) -> Error<'a> {
        let start = cursor.pos() - 1;

        while let Some(c) = cursor.peek().copied() {
            if is_valid_symbol(c) || c.is_ascii_whitespace() {
                break;
            } else {
                cursor.advance();
            }
        }

        let end = cursor.pos();

        // Safety:
        // If some character was the beggining of a utf-8 sequence it must end because the
        // iteration stops at a valid ascii character.
        // start cannot be the middle of a sequence because it's either the first character
        // of a &str, or it's preceeded by a valid ascii symbol.
        let slice = unsafe { std::str::from_utf8_unchecked(&cursor[start..end]) };

        Error {
            kind: ErrorKind::InvalidSequence(slice),
            position: start..end,
        }
    }

    fn pprint(&self, input: &str) {
        println!();

        match self.kind {
            ErrorKind::InvalidSequence(seq) => {
                let col = self.position.start;
                let space_pad = " ".repeat(col);
                let arrow_pad = "^".repeat(seq.chars().count());

                eprintln!("error: invalid sequence '{seq}' on column {col}");
                eprintln!("\t{}", input.trim_end());
                eprintln!("\t{space_pad}{arrow_pad} invalid sequence");
            }
        }
    }
}

pub type LexerResult<'a> = Result<TokenStream<'a>, Vec<Error<'a>>>;

pub fn tokenize(input: &str) -> LexerResult {
    let mut stream = Vec::<Token>::new();
    let mut errors = Vec::<Error>::new();

    let mut cursor = Cursor::new(input.as_bytes());

    while let Some(c) = cursor.advance().copied() {
        match c {
            b' ' | b'\t' | b'\r' | b'\n' => continue,

            b'+' => stream.push(Token::plus(&cursor)),

            b'-' => stream.push(Token::minus(&cursor)),

            b'*' => stream.push(Token::multiply(&cursor)),

            b'/' => stream.push(Token::divide(&cursor)),

            b'(' => stream.push(Token::lparen(&cursor)),

            b')' => stream.push(Token::rparen(&cursor)),

            d if d.is_ascii_digit() => stream.push(Token::number(&mut cursor)),

            _ => errors.push(Error::invalid_sequence(&mut cursor)),
        }
    }

    if errors.is_empty() {
        stream.push(Token::eof(&cursor));
        Ok(stream)
    } else {
        Err(errors)
    }
}

pub fn error(input: &str, errors: &[Error]) {
    for error in errors {
        error.pprint(input)
    }
}
