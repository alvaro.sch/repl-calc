use derive_more::Deref;

#[derive(Debug, Clone, PartialEq)]
pub enum AcceptError<'a, T> {
    StreamExhausted,
    DidNotMatch(&'a T),
}

pub type AcceptResult<'a, T> = Result<&'a T, AcceptError<'a, T>>;

#[derive(Debug, Clone, Deref)]
pub struct Cursor<'a, T> {
    #[deref]
    stream: &'a [T],
    pos: usize,
}

impl<'a, T> Cursor<'a, T> {
    pub fn new(stream: &'a [T]) -> Self {
        Self { stream, pos: 0 }
    }

    pub fn pos(&self) -> usize {
        self.pos
    }

    pub fn peek(&self) -> Option<&T> {
        self.stream.get(self.pos)
    }

    pub fn peek_next(&self) -> Option<&T> {
        self.stream.get(self.pos + 1)
    }

    pub fn advance(&mut self) -> Option<&T> {
        let current = self.stream.get(self.pos);

        if current.is_some() {
            self.pos += 1;
        }

        current
    }

    pub fn accept<P>(&mut self, pred: P) -> AcceptResult<'a, T>
    where
        P: Fn(&T) -> bool,
    {
        let Some(current) = self.stream.get(self.pos) else {
            return Err(AcceptError::StreamExhausted);
        };

        if pred(current) {
            self.pos += 1;
            Ok(current)
        } else {
            Err(AcceptError::DidNotMatch(current))
        }
    }
}
