use std::io::{self, Write};

use repl_calc::*;

type ProcessResult = Result<(), i32>;

fn main() -> ProcessResult {
    let mut args = std::env::args().skip(1);

    let opt = args.next();
    let arg = args.next();

    match (opt.as_deref(), arg.as_deref()) {
        (Some("-repl"), None) | (None, None) => repl(),
        (Some("-stdin"), None) => onepass(),
        (Some("-eval"), Some(expr)) => eval(expr),
        (opt, arg) => {
            eprint!("invalid argument pair '{opt:?}' '{arg:?}'");
            Err(1)
        }
    }
}

fn eval(expr: &str) -> ProcessResult {
    process(expr)
}

fn onepass() -> ProcessResult {
    let mut buffer = String::new();
    let _ = io::stdin().read_line(&mut buffer);

    process(&buffer)
}

fn repl() -> ProcessResult {
    let mut last_result = Ok(());
    let mut buffer = String::new();

    loop {
        buffer.clear();

        print!("\nin > ");

        let _ = io::stdout().flush();

        match io::stdin().read_line(&mut buffer) {
            Ok(0) => break,
            Err(_) => break,
            _ => {}
        };

        if buffer.trim() == "exit" {
            break;
        }

        last_result = process(&buffer);
    }

    last_result
}

fn process(buffer: &str) -> ProcessResult {
    let ts = match lexer::tokenize(buffer) {
        Ok(ts) => ts,
        Err(errors) => {
            lexer::error(buffer, &errors);
            return Err(1);
        }
    };

    let ast = match parser::parse_to_ast(&ts) {
        Ok(expr) => expr,
        Err(error) => {
            parser::error(buffer, &error);
            return Err(1);
        }
    };

    let lit = ast.interpret();

    println!("out > {lit}");

    Ok(())
}
